
'use strict';


/**
 * Code
 * -------------------------------------------------------------------
 */


jQuery(document).ready(function() {


/*_____________ Header _____________*/

    /*--- sticky header ---*/

    jQuery(window).on('scroll',function(){
        var pheaderHeight;
        if(jQuery('.aod-page-header').length){
            pheaderHeight = jQuery('.aod-page-header').height();
        }
        if (jQuery(this).scrollTop() > pheaderHeight){  
        jQuery('.aod-page-header').addClass("sticky");
        }
        else{
        jQuery('.aod-page-header').removeClass("sticky");
        }
    });


    /*--- mobile menu ---*/ 

    if(jQuery('.mobile-nav').length) {
        var sj_mb_nav = jQuery('.mobile-nav');
        sj_mb_nav.each(function(){
            var sj_mbn_pull = jQuery(this).children('span'),
                sj_mbn_ul   = jQuery(this).children('.mobile-menu');

            sj_mbn_pull.on('click',function () {
                sj_mbn_ul.slideToggle('normal').toggleClass('active');
            });

        });
    }


/*_____________ Scroll to Top _____________*/   


    jQuery(window).on('scroll',function(){
        if (jQuery(this).scrollTop() > 200) {
            jQuery('.scrollup').fadeIn();
        } else {
            jQuery('.scrollup').fadeOut();
        }
    }); 
    jQuery('.scrollup').on('click',function(){
        jQuery("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });


/*_____________ Owl Carousel _____________*/


    var owl1 = jQuery(".owl-carousel-1");
    if(owl1.length){
        owl1.each(function(){
            var o1_pag  = jQuery(this).data('pagination'),
                o1_nav  = jQuery(this).data('navigation'),
                o1_auto = jQuery(this).data('autoplay');

            jQuery(this).owlCarousel({
                singleItem: true, 
                slideSpeed: 600,
                autoPlay: o1_auto,
                pagination: o1_pag,
                navigation: o1_nav,
                addClassActive: true,
                afterInit: function(){
                    jQuery('.aod-carousel-1').find('.loading').hide();    
                }
            });
        });
    }

    var owl2 = jQuery(".owl-carousel-2");
    if(owl2.length){
        owl2.each(function(){
            var o2_item       = jQuery(this).data('item'),
                o2_item_mb    = jQuery(this).data('item-mobile'),
                o2_pag        = jQuery(this).data('pagination'),
                o2_nav        = jQuery(this).data('navigation'),
                o2_auto       = jQuery(this).data('autoplay'),
                o2_item_count = jQuery(this).children().length;

            if(o2_item_count > o2_item){
                jQuery(this).parent().addClass('has-nav');
            }

            jQuery(this).owlCarousel({
                items: o2_item,
                itemsMobile: o2_item_mb,
                slideSpeed: 300,
                autoPlay: o2_auto,
                pagination: o2_pag,
                navigation: o2_nav,
                navigationText: false
            });
        });
    }


/*_____________ Validate Form _____________*/
    
    var sj_form = jQuery('.aod-form-contact');
    if(sj_form.length) {
        jQuery(sj_form).each(function(){

            jQuery(this).validate({
                // Add requirements to each of the fields
                rules: {
                    name: {
                        required: true,
                        minlength: 6
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: {
                        required: true,
                        minlength: 10
                    }
                },
                // Specify what error messages to display
                // when the user does something horrid
                messages: {
                    name: {
                        required: "Please enter your name.",
                        minlength: jQuery.format("At least {0} characters required.")
                    },
                    email: {
                        required: "Please enter your email.",
                        email: "Please enter a valid email."
                    },
                    message: {
                        required: "Please enter a message.",
                        minlength: jQuery.format("At least {0} characters required.")
                    }
                },
                submitHandler: function () { 

                    

                    var cNameVal  = '"' + jQuery(sj_form).find("input[name='name']").val() + '"',
                        cEmailVal = '"' + jQuery(sj_form).find("input[name='email']").val() + '"',
                        cMeslVal  = '"' + jQuery(sj_form).find("textarea").val() + '"';


                    var url = window.location.origin + '/AODWebAPI/api/Contact/SendMail';
                    var data = {
                        CustomerName: cNameVal,
                        CustomerEmail: cEmailVal,
                        Message: cMeslVal
                    };

                    jQuery.ajax({
                        url: url,
                        type: 'POST',
                        async: false,
                        cache: false,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify(data),
                        success: function (response) {
                            jQuery('.aod-form-contact').addClass('sent');
                            jQuery('.contact-box-notice').addClass('active');
                            sj_form.find("input[type!='submit'], textarea").val('');
                        }
                    });
                }
            });

        });
    }


/*_____________ Masonry _____________*/


    /*--- module-team ---*/

    if(jQuery('.module-team-1').length){
        jQuery('.module-team-1').each(function(){
            var sj_teamMasonry = jQuery(this).find('.masonry-wrap');
            sj_teamMasonry.imagesLoaded(function(){
                sj_teamMasonry.masonry({
                    columnWidth: 1,
                    gutterr: -1,
                    itemSelector: '.ms-item'
                });
                sj_teamMasonry.masonry('bindResize');
            });
        });
    }

    /*--- module-timeline-masonry ---*/
    
    if(jQuery('.module-timeline-masonry').length){
        jQuery('.module-timeline-masonry').each(function(){

            var sj_masonry = jQuery(this).find('.timeline-content-wrap');
            sj_masonry.imagesLoaded(function(){
                sj_masonry.masonry({
                    columnWidth: 1,
                    itemSelector: '.tl-item'
                });
                sj_masonry.masonry('bindResize');
            });
            var w_w     = jQuery(window).width(),
                m_w     = jQuery('.module-timeline-masonry').width(),
                s_w     = (w_w - m_w)/2,
                tl_item = jQuery(this).find('.tl-item');

            window.setTimeout((function() {
                tl_item.each(function(){
                    var tl_item_osl = jQuery(this).offset().left;
                    if(tl_item_osl > s_w){
                        jQuery(this).addClass('style-01');
                    }
                });
                jQuery('.timeline-content-wrap').addClass('active');
            }),100);


        });
   
        
    }


/*_____________ Fitvids _____________*/

    var fitVideo = jQuery('.js-fitvid');
    if(fitVideo.length){
        fitVideo.each(function(){
            jQuery(this).fitVids();
        });
    }

/*_____________ Custom _____________*/

    
    /*--- smooth scroll ---*/

    var sj_sm_scroll = jQuery('.sj-scroll-btn');
    if(sj_sm_scroll.length){
        sj_sm_scroll.each(function(){
            jQuery(this).on('click',function(event) {
                event.preventDefault();
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 60
                        }, 800);
                        return false;
                    }
                }
            });
        });
    }

    /*--- order-list ---*/

    function sj_order_list(){
        var sj_ulod = jQuery('.aod-order-list');
        if(sj_ulod.length){
            sj_ulod.each(function(){
                var sj_liod = jQuery(this).children();
                sj_liod.each(function() {
                    var sj_liod_index = jQuery(this).index() + 1;
                    if(sj_liod_index < 10){
                        sj_liod_index = '0' + sj_liod_index;
                    }
                    var sj_liod_num = sj_liod_index + '.';
                    jQuery(this).find('.aod-order-num').html(sj_liod_num);
                });
            });
        }
    }
    sj_order_list();
    

    /*--- Collaspe ---*/

    var aod_collapse = jQuery('.aod-collapse');
    if(aod_collapse.length){
        aod_collapse.addClass('aod-collapse-in');
    }
    
    /* accordion */
    var aod_accordion = jQuery('.aod-accordion');
    if(aod_accordion.length){
        aod_accordion.each(function(){

            var panel = jQuery(this).find('.collapse-panel');

            panel.each(function(){
                var panel_heading   = jQuery(this).find('.panel-heading'),
                    panel_content   = jQuery(this).find('.panel-content');

                if (jQuery(this).hasClass('active')) {
                    jQuery(this).find('.panel-content').css('display','block');
                }
                panel_heading.on('click', function(e){
                    e.preventDefault();
                    if (!jQuery(this).closest(panel).hasClass('active')) {
                        jQuery(this).closest(aod_accordion).find('.active').removeClass('active');
                        jQuery(this).closest(aod_accordion).find('.panel-content').slideUp();
                        jQuery(this).closest(panel).addClass('active');
                        jQuery(this).closest(panel).find(panel_content).slideDown();
                    }
                });
            });

        });
    }

    /* toggle */
    var aod_toggle = jQuery('.aod-toggle');
    if(aod_toggle.length){
        aod_toggle.each(function(){

            var panel = jQuery(this).find('.collapse-panel');

            panel.each(function(){
                var panel_heading   = jQuery(this).find('.panel-heading'),
                    panel_content   = jQuery(this).find('.panel-content');
                if (jQuery(this).hasClass('active')) {
                    jQuery(this).find('.panel-content').css('display','block');
                }
                panel_heading.on('click', function(e){
                    e.preventDefault();
                    if (jQuery(this).closest(panel).hasClass('active')) {
                        jQuery(this).closest(panel).removeClass('active');
                        jQuery(this).closest(panel).find(panel_content).slideUp();
                    }
                    else{
                        jQuery(this).closest(panel).addClass('active');
                        jQuery(this).closest(panel).find(panel_content).slideDown();
                    }
                });
            });

        });
    }

    /*--- background-item ---*/

    var bg_item = jQuery('.sj-bg-item');
    if(bg_item.length){
        bg_item.each(function(){
            var bg_item_content = jQuery(this).data('bg');
            jQuery(this).css('background-image', 'url("' + bg_item_content  + '")');
        })
    }

    /*--- dropdown ---*/

    var sj_ddn = jQuery('.sj-dropdown');
    if(sj_ddn.length) {
        jQuery(sj_ddn).each(function() {
            var sj_this        = jQuery(this),
                sj_ddn_btn     = sj_this.find('.sj-dropdown-btn'),
                sj_ddn_closest = sj_this.closest('.sj-dropdown');
            sj_ddn_btn.on('click', function (e){
                e.preventDefault();
                sj_ddn_closest.toggleClass('active');
            });
            jQuery("html").mouseup(function (e){
                if (!sj_this.is(e.target) && sj_this.has(e.target).length === 0){
                    sj_ddn_closest.removeClass('active');
                }
            });
        });
    }

    /*--- contact ---*/

    var contactBtn = jQuery('.contact-btn');
     if(contactBtn.length){
        contactBtn.each(function(){
            jQuery(this).on('click', function(){
                jQuery(this).closest('nav').find('li').removeClass('active');
                jQuery(this).parent('li').addClass('active');
                jQuery('.aod-form-contact').find('.aod-btn-4').trigger('click');
                jQuery('.mobile-menu').slideUp().removeClass('active');
            });
        })
    }

    /*--- contact-box-notice ---*/

    var contactBoxNotice = jQuery('.contact-box-notice');
     if(contactBoxNotice.length){
        contactBoxNotice.each(function(){
            var ctBNclose = jQuery(this).find('.close-btn');
            ctBNclose.on('click', function(){
                jQuery(this).closest(contactBoxNotice).removeClass('active');
            });

        })
    }

/*_____________ Wow _____________*/

    if(jQuery('.wow').length){
        var wow = new WOW({mobile: false,offset: 10});
        wow.init();
    }


/*_____________ MatchHeight _____________*/

        

    function mh(){
        jQuery('.aod-mh').children().matchHeight();
    }
    if(jQuery('.aod-mh').length){
        mh();
        window.setTimeout((function() {
            mh();
        }),100);
    }

});


/*_____________ Black & White _____________*/

jQuery(window).load(function(){
    var BWitems1 = jQuery('.module-team-1').find('.item-thumb');
    if(jQuery('.module-team-1').length){
        jQuery(BWitems1).each(function(){
            jQuery(this).BlackAndWhite({
                hoverEffect : true, // default true
                // set the path to BnWWorker.js for a superfast implementation
                webworkerPath : false,
                // to invert the hover effect
                invertHoverEffect: false,
                // this option works only on the modern browsers ( on IE lower than 9 it remains always 1)
                intensity:1,
                speed: { //this property could also be just speed: value for both fadeIn and fadeOut
                    fadeIn: 200, // 200ms for fadeIn animations
                    fadeOut: 800 // 800ms for fadeOut animations
                },
                onImageReady:function(img) {
                    // this callback gets executed anytime an image is converted
                }
            });
        });
    }
});